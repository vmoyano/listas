
package listas;

public class ListaInt {
	NodoInt primero;

	public void imprimir() {
		System.out.print("[ ");
		NodoInt n = this.primero;
		while (n != null) {
			System.out.print(n.elemento + " ");
			n = n.siguiente;
		}
		System.out.println("]");
	}

	public void agregarAdelante(int x) {
		NodoInt nuevo = new NodoInt();
		nuevo.elemento = x;
		nuevo.siguiente = this.primero;
		this.primero = nuevo;
	}

	// EJ 1
	public boolean pertenece(int x) {
		NodoInt n = this.primero;
		while (n != null) {
			if (n.elemento == x) {
				return true;
			}
			n = n.siguiente;
		}
		return false;
	}

	// EJ 4
	public void quitarTodos(int x) {
		NodoInt n = this.primero;
		// Analizo desde el 2do numero
		while (n != null && n.siguiente != null) {
			if (n.siguiente.elemento == x) {
				n.siguiente = n.siguiente.siguiente;
			} else {
				n = n.siguiente;
			}
		}
		// Chequear el primero
		if (this.primero != null && this.primero.elemento == x) {
			this.primero = this.primero.siguiente;
		}
	}
	
	// EJ 8
	public void quitarDuplicados() {
		ListaInt nueva = new ListaInt();
		NodoInt n = this.primero;
		while (n != null) {
			if (!nueva.pertenece(n.elemento)) {
				nueva.agregarAtras(n.elemento);
				n = n.siguiente;
			} else {
				n = n.siguiente;
			}
		}
		this.primero = nueva.primero;
	}

	public void agregarAtras(int x) {
		NodoInt n = this.primero;
		NodoInt nuevo = new NodoInt();
		nuevo.elemento = x;
		if (n == null) {
			this.primero = nuevo;
		} else {
			while (n.siguiente != null) {
				n = n.siguiente;
			}
			n.siguiente = nuevo;
		}
	}

	// EJ 3
	public void insertarOrdenado(int x) {
		if (this.primero == null || this.primero.elemento >= x) {
			this.agregarAdelante(x);
			return;
		}
		NodoInt n = this.primero;
		while(n!=null && n.siguiente != null &&
			  n.elemento < x && n.siguiente.elemento < x) {
				n = n.siguiente;
		}
		//Tengo que agregar el nuevo despues del n
		NodoInt nuevo = new NodoInt();
		nuevo.elemento = x;
		nuevo.siguiente = n.siguiente;
		n.siguiente = nuevo;
		
	}
	
	//DescomponerPares
	public void descomponerPares() {
		NodoInt n = this.primero;
		while(n!=null) {
			if(n.elemento % 2 == 0) {
				NodoInt nuevo = new NodoInt();
				nuevo.elemento = n.elemento*2;
				nuevo.siguiente = n.siguiente;
				n.siguiente = nuevo;
				n.elemento = n.elemento/2;
				n = nuevo.siguiente;
			} else {
				n = n.siguiente;
			}
		}
	}
	//El nodo n recorre la lista 1 vez.
	//Tanto el if como el else son O(1).
	//Queda de complejidad lineal.
	
	//filtrar elementos en rango
	public void filtrarElementosEnRango(int k, int m) {
		NodoInt n = this.primero;
		// Analizo desde el 2do numero
		while (n != null && n.siguiente != null) {
			if (n.siguiente.elemento<= k || n.siguiente.elemento>=m) {
				n.siguiente = n.siguiente.siguiente;
			} else {
				n = n.siguiente;
			}
		}
		// Analizo el primero
		if(this.primero != null) {
			if(this.primero.elemento <= k || this.primero.elemento >= m) {
				this.primero = this.primero.siguiente;
			}
		}
	}
	// El metodo recorre la lista con el while 1 vez, O(n).
	// Luego se fija en el primero, eso es O(1).
	// Queda de complejidad lineal.
	
	
}
